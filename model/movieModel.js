const { Schema, model } = require('mongoose');
const { database } = require('../utils/key');

var ratingsSchema = new Schema({
  
        Source:{
           type: String,
           required: false,
           trim: true
        },
        Value:{
            type: String,
            required: false,
            trim: true
        }
    
 
}, { _id : false });

const movieSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true,
    },
    year: {
        type: String,
        required: true,
        trim: true,
    },
    released: {
        type: String,
        required: true,
        trim: true,
    },
    genre: {
        type: String,
        required: true,
        trim: true,
    },
    directors:{
        type: String,
        required: true,
        trim: true,
    },
    actors:{
        type: String,
        required: true,
        trim: true,
    },
    plot:{
        type: String,
        required: true,
        trim: true,
    },
    ratings: [ratingsSchema]


})
module.exports = model('Movie', movieSchema, database.collectionName );