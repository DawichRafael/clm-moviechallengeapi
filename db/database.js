const mongoose = require('mongoose');

const { database } = require('../utils/key');

mongoose.connect(database.URI,{
    useNewUrlParser: true
});
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Database error:'));
 
db.once('open', function() {
  console.log("Database connected");
});

module.exports = db;