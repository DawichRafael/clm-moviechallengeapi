const koa = require('koa');
const koaRouter = require('koa-router');
const axios = require('axios');
const swagger = require("swagger2");
const movieSchema = require('./model/movieModel');
const json = require('koa-json');
const bodyParser = require('koa-bodyparser');
const { ui } = require("swagger2-koa");
const { externalAPI } = require('./utils/key');


// DataBase
require('./db/database');

// Configurations Koa, Routes, Swagger, KEY
const app = new koa();
const router = new koaRouter();
const swaggerDocument = swagger.loadDocumentSync("./utils/api.yaml");


// Routes
router.get('/getMovie', async (ctx) => {
    let result;
    ctx.query.year ? result = await axios.get(`${externalAPI.host}?apikey=${externalAPI.API_KEY}&t=${ctx.query.title}&y=${ctx.query.year}`) : 
    result = await axios.get(`https://www.omdbapi.com/?apikey=${externalAPI.API_KEY}&t=${ctx.query.title}`);
    
    if(result.data.Response === 'True') {

      let movie = new movieSchema({
          title: result.data.Title,
          year: result.data.Year,
          released: result.data.Released,
          genre: result.data.Genre,
          directors: result.data.Director,
          actors: result.data.Actors,
          plot: result.data.Plot,
          ratings: result.data.Ratings
        });
      
        let movieExist = await getMovieByTitle(result.data.Title);
  
        if(!movieExist) {
          movie.save((err, moviedb) => {
            if(err)return console.error(err);
            console.log(moviedb);
          });
        } 
        else {
          console.log('Pelicula no insertada ya esta en la base de datos');
        }
        ctx.status = 200;
        ctx.body = movie;
    }else{
      ctx.status = 500;
      ctx.body = 'Pelicula no encontrada';
    }    
});

router.get('/listMovie', async (ctx) => {
    let result = await getListMovie();
    if(result){
      const listMovies = paginationMovie(result, ctx.query.page);
      ctx.status = 200;
      ctx.body =  listMovies;
      
    }else{
      ctx.status = 500;
      ctx.body = 'No hay ninguna pelicula en nuestra lista';
    }
 
});

router.post('/updateMovie', async (ctx) => {
  const movieRequest = ctx.request.body;
  let movieExist = await getMovieByTitle(movieRequest.title);

  if(movieExist) {
    const movieUpdated = await updateMovie(movieExist,movieRequest.find,movieRequest.replace);

    ctx.status = 200;
    ctx.body = movieUpdated;
  }else {
    ctx.status = 500;
    ctx.body = 'Pelicula no encontrada';
  }
})
 
// Functions
let getListMovie = () => {
  return movieSchema.find({}).then(data => { return data } )
}

let getMovieByTitle = (titleFind) =>{
  return movieSchema.findOne({ title: titleFind}).then(data => {return data});
}

let paginationMovie= (listMovies, current_page) => {
  let page = current_page || 1,
	per_page = 5,
	offset = (page - 1) * per_page,

	paginatedItems = listMovies.slice(offset).slice(0, 5),
	total_pages = Math.ceil(listMovies.length / per_page);

	return {
		page: page,
		per_page: per_page,
		pre_page: page - 1 ? page - 1 : null,
		next_page: (total_pages > page) ? page + 1 : null,
		totalMovies: listMovies.length,
		total_pages: total_pages,
		movies: paginatedItems
	};
}

let updateMovie = (movieFind, oldValue, newValue) => {
  let newMovie = {...movieFind };
  movieFind.plot = movieFind.plot.replace(oldValue, newValue);

  return movieSchema.findByIdAndUpdate(movieFind, newMovie,{new: true}).then(data => {return data});
}
// API configuration 
app.use(json())
.use(ui(swaggerDocument, "/swagger"))
.use(bodyParser())
.use(router.routes())
.use(router.allowedMethods())
.listen(3000, () =>{
     console.log('MovieChallenge API is runing...');
});


 