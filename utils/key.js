module.exports = {
    database:{
        URI: 'mongodb://localhost/moviedb',
        collectionName: 'Movies'
    },
    externalAPI: {
        host: 'https://www.omdbapi.com/',
        API_KEY: 'd9f35772'
    }
}